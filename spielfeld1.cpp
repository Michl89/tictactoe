#include "spielfeld1.h"
#include "ui_spielfeld1.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "spiel_logik.h"

#include <QMessageBox>
#include <QDebug>


Spielfeld1::Spielfeld1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Spielfeld1)
{
    ui->setupUi(this);
    ui->Spielgitter->setPixmap(QPixmap("C:/Users/singe/Desktop/Projekt Game TicTacToe/TicTacToe/Gitter.png"));
    s = new Spiel_Logik(feld);

}

Spielfeld1::~Spielfeld1()
{
    delete ui;
}

void Spielfeld1::on_Beenden_clicked()
{
  QMessageBox::information(this,"Message","Das Spiel wurde beendet",QMessageBox::Ok);
  close();
}

void Spielfeld1::on_GittButton_1_clicked()
{
    if(block1 == true) // ungeklicked ist block true und kann geklickt werden
    {
         counter = counter % 2 + 1;
         s->setField(0, 0, counter);// greift auf das Vectorfeld zu und übergibt den Counter


       if (counter%2 == 0)
        {

            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug"); 
            ui->GittButton_1->setPalette(labelPalette);
            ui->GittButton_1->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_1->setText("O");
        }

        unentschieden ++; // Zählt auf 9, sobald Zahl erreicht = unentschieden
        Unentschieden();

        s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[0][0];
        block1 = false; // sobald block geklickt wurde wird dieser auf false gesetzt und gesperrt

    }
}


void Spielfeld1::on_GittButton_2_clicked()
{
    if(block2 == true)
    {
        counter = counter % 2 + 1;
        s->setField(0, 1, counter);
        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_2->setPalette(labelPalette);
            ui->GittButton_2->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_2->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[0][1];
        block2 =false;
    }
}

void Spielfeld1::on_GittButton_3_clicked()
{
    if(block3 == true)
    {
        counter = counter % 2 + 1;
        s->setField(0, 2, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue); //Färbt den Text blau bzw. rot
            ui->label->setPalette(labelPalette); //Färbt den Text blau bzw. rot
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_3->setPalette(labelPalette);
            ui->GittButton_3->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_3->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[0][2];
        block3=false;
    }
}

void Spielfeld1::on_GittButton_4_clicked()
{
    if(block4 == true)
    {
       counter = counter % 2 + 1;
        s->setField(1, 0, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_4->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_4->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[1][0];
        block4=false;
    }
}

void Spielfeld1::on_GittButton_5_clicked()
{
    if(block5 == true)
    {
        counter = counter % 2 + 1;
        s->setField(1, 1, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_5->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_5->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[1][1];
        block5 = false;
    }
}

void Spielfeld1::on_GittButton_6_clicked()
{
    if(block6 == true)
    {
        counter = counter % 2 + 1;
        s->setField(1, 2, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_6->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_6->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[1][2];
        block6 = false;
    }
}

void Spielfeld1::on_GittButton_7_clicked()
{
    if(block7 == true)
    {
        counter = counter % 2 + 1;
        s->setField(2, 0, counter);
        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_7->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_7->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[2][0];
    }
}

void Spielfeld1::on_GittButton_8_clicked()
{
    if(block8 == true)
    {
        counter = counter % 2 + 1;
        s->setField(2, 1, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_8->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_8->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[2][1];
        block8 = false;
    }
}

void Spielfeld1::on_GittButton_9_clicked()
{
    if(block9 == true)
    {
         counter = counter % 2 + 1;
        s->setField(2, 2, counter);

        if (counter%2 == 0)
        {
            labelPalette.setColor(QPalette::WindowText,Qt::blue);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 1 ist am Zug");
            ui->GittButton_9->setText("X");
        }
        else
        {
            labelPalette.setColor(QPalette::WindowText,Qt::red);
            ui->label->setPalette(labelPalette);
            ui->label->setText("Spieler 2 ist am Zug");
            ui->GittButton_9->setText("O");
        }
        unentschieden ++;
        Unentschieden();


         s->GewinnerErmittlung();
        qDebug() <<"Counter: " << counter << "Feld:" << feld[2][2];
        block9 = false;
    }
}

//Methode um die Message "Unentschieden "auszugeben.
void Spielfeld1::Unentschieden()
{
    if(unentschieden==9)
    {
        QMessageBox::information(this,"Message","Das Spiel ist unentschieden",QMessageBox::Ok);
    }
}






