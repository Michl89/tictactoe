#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "spielfeld1.h"
#include "qdebug.h"
#include <QDebug>
#include "spiel_logik.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Habe ich hier händisch eingetragen, da auf dem UI das Bild bei dem Window nicht angezeigt wurde 6.6.2022
    ui->Hintergrund->setPixmap(QPixmap("C:/Users/singe/Desktop/Projekt Game TicTacToe/TicTacToe/Hintergrund Menue.png"));

    //Öffnet zweiten Dialog(Fenster)
    connect(ui->PlayerVsProgramm, SIGNAL(clicked()), this, SLOT(openSecondWindow()));
    connect(ui->Player1VsPlayer2, SIGNAL(clicked()), this, SLOT(openSecondWindow()));


}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_PlayerVsProgramm_clicked()
{
    Spielfeld1 *f = new Spielfeld1();
    f->show();


}


void MainWindow::on_Player1VsPlayer2_clicked()
{
    Spielfeld1 *f = new Spielfeld1();
    f->show();

}

void MainWindow::on_Schliessen_clicked()
{
  qDebug() << "Das Spiel wird beendet";
  QMessageBox::information(this,"Message","Das Programm wird geschlossen",QMessageBox::Ok);
  close();
}



