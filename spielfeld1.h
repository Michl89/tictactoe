#ifndef SPIELFELD1_H
#define SPIELFELD1_H
#include <QObject>
#include <QWidget>
#include "mainwindow.h"
#include "spiel_logik.h"
#include <QVector>

namespace Ui {
class Spielfeld1;
}
class Spielfeld1 : public QWidget
{
    Q_OBJECT

public:
    explicit Spielfeld1(QWidget *parent = nullptr);

    ~Spielfeld1();


    Spiel_Logik *s;
    void Unentschieden();
    QPalette labelPalette; //Färbt den Text blau bzw. rot

    int counter = 0;

private:
    Ui::Spielfeld1 *ui;

    int unentschieden = 0;
    bool block1 = true;
    bool block2 = true;
    bool block3 = true;
    bool block4 = true;
    bool block5 = true;
    bool block6 = true;
    bool block7 = true;
    bool block8 = true;
    bool block9 = true;
    int a = 0;





protected:
     QVector<QVector<int>> feld = { { 0, 0, 0 },
                                    { 0, 0, 0 },
                                    { 0, 0, 0 } };




private slots: // geändert von private in protected

     void on_Beenden_clicked();
     void on_GittButton_1_clicked();
     void on_GittButton_2_clicked();
     void on_GittButton_3_clicked();
     void on_GittButton_4_clicked();
     void on_GittButton_5_clicked();
     void on_GittButton_6_clicked();
     void on_GittButton_7_clicked();
     void on_GittButton_8_clicked();
     void on_GittButton_9_clicked();




};

#endif // SPIELFELD1_H
