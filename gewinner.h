#ifndef GEWINNER_H
#define GEWINNER_H

#include <QDialog>
//#include "spielfeld1.h"


namespace Ui {
class Gewinner;
}

class Gewinner : public QDialog
{
    Q_OBJECT

public:
    explicit Gewinner(QWidget *parent = nullptr);
    ~Gewinner();

private:
    Ui::Gewinner *ui;
};

#endif // GEWINNER_H
