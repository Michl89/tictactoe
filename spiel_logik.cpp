#include "spiel_logik.h"
#include "gewinner.h"
#include "mainwindow.h"
#include "ui_spielfeld1.h"// hinzugefügt 6.9.2022
#include "spielfeld1.h"
#include <QVector>
#include <QDebug>

void Spiel_Logik::winner() // Öffenen des Fensters bei Gewinn (24.08)
{
    Gewinner gewinner;
    gewinner.setModal(true);
    gewinner.exec();
}

/*void Spiel_Logik::PcGegner(int Zufall)  //Nicht fertig Vectorfelder nummern geben für randfunk 03.09.2022
{
    Spielfeld1 Spielfeld2; //Objekt von Spielfeld1 erzeugen, um auf PushButton zuzugreifen 11.9

    srand (time(NULL));

    Zufall = rand()%9 +1;

    switch(Zufall)
    {
        case 1: Spielfeld2.counter = 1; break; //?? Wie nutze ich das hier für den PC-Gegner?? 11.9
        case 2: Spielfeld2.counter = 1; break;
        case 3: Spielfeld2.counter = 1; break;
        case 4: Spielfeld2.counter = 1; break;
        case 5: Spielfeld2.counter = 1; break;
        case 6: Spielfeld2.counter = 1; break;
        case 7: Spielfeld2.counter = 1; break;
        case 8: Spielfeld2.counter = 1; break;
        case 9: Spielfeld2.counter = 1; break;
    }
}*/


Spiel_Logik::Spiel_Logik(QVector<QVector<int>> &infeld)
{
    feld = infeld;
}

int Spiel_Logik::GewinnerErmittlung()
   {

       for (int i = 0; i <= 2; i++)
       {
           if (feld.at(i).at(0) != 0 && feld.at(i).at(0) == feld.at(i).at(1) && feld.at(i).at(0) == feld.at(i).at(2))
           {
              //PcGegner(Zufall);

               winner();

               return feld.at(i).at(0);
           }
       }
       for (int j = 0; j <= 2; j++)
       {
           if (feld.at(0).at(j) != 0 && feld.at(0).at(j) == feld.at(1).at(j) && feld.at(0).at(j) == feld.at(2).at(j))
           {
              //PcGegner(Zufall);

               winner();

               return feld.at(0).at(j);
           }
       }

       if (feld.at(0).at(0) != 0 && feld.at(0).at(0) == feld.at(1).at(1) && feld.at(0).at(0) == feld.at(2).at(2))
       {
          //PcGegner(Zufall);

           winner();

           return feld.at(0).at(0);
       }

       if (feld.at(0).at(2) != 0 && feld.at(0).at(2) == feld.at(1).at(1) && feld.at(0).at(2) == feld.at(2).at(0))
       {
          //PcGegner(Zufall);

           winner();

           return feld.at(0).at(2);
       }

       return 0;
   }
