#ifndef SPIEL_LOGIK_H
#define SPIEL_LOGIK_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QString>
#include <stdlib.h> //pc zufällig feld wählen 5.9.2022
#include <time.h> //pc zufällig feld wählen 5.9.2022
#include "gewinner.h"
//#include "ui_spielfeld1.h"




class Spiel_Logik
{
public:

    Spiel_Logik(QVector<QVector<int>> &infeld);
    int GewinnerErmittlung();
    void setField(int x, int y, int val){
        feld[x][y] = val;
    }
    //void PcGegner(int Zufall);


private:
    QVector<QVector<int>> feld;

    void winner();
    int Zufall; //pc zufällig feld wählen



};

#endif // SPIEL_LOGIK_H



